from httpx import AsyncClient
import uvicorn
from fastapi import Depends, FastAPI, Query
from src import Settings, load_best_route, get_settings, read_lat_lng, get_client, Coord3

app = FastAPI()


@app.get(
    "/",
    name="Get best route",
    description="Get best route based on source location and target location.",
)
async def get_best_route(
    source: str = Query(...,
                        description="Source location in format `lat,lng`."),
    target: str = Query(...,
                        description="Target location in format `lat,lng`."),
    client: AsyncClient = Depends(get_client),
    settings: Settings = Depends(get_settings)
) -> list[Coord3]:
    source_coord = read_lat_lng(source)
    target_coord = read_lat_lng(target)

    return await load_best_route(client, settings.dgis_api_key, source_coord, target_coord)


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
