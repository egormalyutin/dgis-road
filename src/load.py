"""Several tools to reduce count of requests required to load data from APIs."""

from typing import TypeVar


T = TypeVar("T")


def compress_unique(l: list[T]) -> tuple[list[T], list[int]]:
    """Compress list so it will include only unique values, but order of values can be restored using decompress_unique."""

    res = []
    order = []
    first_occurences = dict()

    for item in l:
        if item not in first_occurences:
            first_occurences[item] = len(res)
            res.append(item)
        order.append(first_occurences[item])

    return res, order


def decompress_unique(packed: list[T], order: list[int]) -> list[T]:
    """Decompress values using order returned by compress_unique."""

    res = []
    for item in order:
        res.append(packed[item])
    return res


def from_chunks(chunks: list[list[T]]) -> tuple[list[T], list[int]]:
    """Convert list of chunks to a single list and sizes of chunks. After that, list can be restored using to_chunks."""

    return flatten(chunks), list(map(len, chunks))


def to_chunks(flat: list[T], sizes: list[int]) -> list[list[T]]:
    """Break list into chunks of defined sizes."""
    cur = 0
    for size in sizes:
        yield flat[cur:cur+size]
        cur += size


def to_chunks_fixed(l: list[T], n: int) -> list[list[T]]:
    """Divide list into chunks of size n."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


def flatten(l: list[list[T]]) -> list[T]:
    """Flatten list."""
    return [x for item in l for x in item]


# Decorators


def wrap_process_compressed(fn):
    """Wrap async function using compress_unique and decompress_unique."""
    async def wrapped(array):
        packed, mapping = compress_unique(array)
        res = await fn(packed)
        return decompress_unique(res, mapping)
    return wrapped


def wrap_process_chunks(fn):
    """Wrap async function using from_chunks and to_chunks."""
    async def wrapped(array):
        flat, sizes = from_chunks(array)
        res = await fn(flat)
        return list(to_chunks(res, sizes))
    return wrapped


def wrap_process_in_fixed_chunks(fn, n):
    """Wrap async function using to_chunks_fixed and flatten."""
    async def wrapped(array):
        res = []
        for chunk in to_chunks_fixed(array, n):
            res += await fn(chunk)
        return res
    return wrapped
