from httpx import AsyncClient
from .util import Coord2
from .load import wrap_process_chunks, wrap_process_compressed, wrap_process_in_fixed_chunks


async def load_elevations(client: AsyncClient, coords: list[list[Coord2]]):
    async def loader(coords: list[Coord2]):
        query = '|'.join(str(lat) + ',' + str(lng) for lat, lng in coords)

        res = await client.post(
            "https://api.opentopodata.org/v1/srtm30m",
            data={"locations": query}
        )

        return [
            coord + (item["elevation"], )
            for coord, item in zip(coords, res.json()["results"])
        ]

    return await wrap_process_chunks(
        wrap_process_compressed(wrap_process_in_fixed_chunks(loader, 100))
    )(coords)
