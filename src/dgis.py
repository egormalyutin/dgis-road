import httpx

from .elevation import load_elevations
from .util import Coord2, Route2
from .calculate import pick_best_route
from geomet import wkt


def read_route(alternative) -> Route2:
    coords = []

    for man in alternative["maneuvers"]:
        if man["type"] == "end":
            continue

        for geometry in man["outcoming_path"]["geometry"]:
            path = wkt.loads(geometry["selection"])
            for lng, lat in path["coordinates"]:
                coords.append((lat, lng))

    return coords


def read_routes(resp) -> list[Route2]:
    return list(map(read_route, resp["result"]))


async def load_routes(client: httpx.AsyncClient, api_key: str, a: Coord2, b: Coord2):
    return (await client.post(
        "https://routing.api.2gis.com/carrouting/6.0.1/global",
        params={"key": api_key},
        json={
            "alternative": 2,
            "type": "jam",
            "points": [
                {
                    "start": True,
                    "type": "walking",
                    "x": a[1],
                    "y": a[0]
                },
                {
                    "start": False,
                    "type": "walking",
                    "x": b[1],
                    "y": b[0],
                }
            ]
        }
    )).json()


async def load_best_route(client: httpx.AsyncClient, dgis_api_key: str, a: Coord2, b: Coord2):
    routes = read_routes(await load_routes(client, dgis_api_key, a, b))
    return pick_best_route(await load_elevations(client, routes))
