from fastapi import HTTPException

import httpx

Coord2 = tuple[float, float]
Coord3 = tuple[float, float, float]
Route2 = list[Coord2]
Route3 = list[Coord3]


async def get_client():
    async with httpx.AsyncClient(timeout=30) as client:
        yield client


def read_lat_lng(str: str) -> Coord2:
    split = str.split(',')
    if len(split) != 2:
        raise HTTPException(status_code=422, detail="Incorrect coordinate")

    try:
        lat = float(split[0])
        lng = float(split[1])
    except (ValueError):
        raise HTTPException(status_code=422, detail="Incorrect coordinate")

    return lat, lng
