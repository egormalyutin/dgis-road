from math import sqrt
from typing import Iterable
from .util import Coord3, Route3
from geopy.distance import distance as geopy_distance


def cost(a: Coord3, b: Coord3):
    x = geopy_distance(a[:2], b[:2]).m
    y = b[2] - a[2]
    l = sqrt(x ** 2 + y ** 2)

    if x == 0:
        return 0

    return max(0, 1 + y/x) * l


def cost_route(route: Iterable[Coord3]):
    res = 0
    last = None
    for coord in route:
        if last is not None:
            res += cost(last, coord)
        last = coord
    return res


def pick_best_route(routes: Iterable[Route3]):
    return min(routes, key=cost_route)
