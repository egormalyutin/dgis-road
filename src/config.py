from functools import lru_cache
from pydantic import BaseSettings


class Settings(BaseSettings):
    dgis_api_key: str

    class Config:
        env_file = 'dgis-road.env'
        env_file_encoding = 'utf-8'


@lru_cache()
def get_settings():
    return Settings()
